import { GraphQLObjectType, GraphQLString, GraphQLNonNull } from 'graphql';

export default new GraphQLObjectType({
    name: 'MutationRoot',
    fields: () => ({
        hello: {
            type: GraphQLNonNull(GraphQLString),
            resolve() {
                return 'Hello world !'
            },
        }
    })
})