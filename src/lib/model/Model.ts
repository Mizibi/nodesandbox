import * as sequelize from 'sequelize'
import { SequelizeModel } from '@lib/model/sequelize'
import { GQLModel } from '@lib/model/graphql'

// Definition
export interface ModelDefinition {
    name: string,
    table: string,
    columns: Columns,
    timestamps: boolean,
}

// Columns
export interface ColumnBase {
    type: any/*SequelizeType*/,
    allowNull?: boolean,
    defaultValue?: any,
}
export interface Column extends ColumnBase {
    exposed: boolean | string/*alias*/,
}
export type Columns = { [name: string]: Column };

/**
 * Class Model
 * Associate Sequelize to GraphQL
 */
export class Model {
    private _sequelizeModel: SequelizeModel
    private _gqlModel: GQLModel

    constructor(public definition: ModelDefinition) {
        this.processDefinitionColumns()

        this._sequelizeModel = new SequelizeModel(this)
        this._gqlModel = new GQLModel(this)
    }

    /**
     * Transform definition column to database columns
     * Add some if necessary (id, created_at, updated_at)
    */
    private processDefinitionColumns() {
        const { timestamps } = this.definition;

        // Add id
        this.setColumnIfNotDefined('id', {
            type: sequelize.INTEGER,
            allowNull: false,
            exposed: true,
        });

        // Add timestamps if definition requires it
        if (timestamps) {
            const definition = { type: sequelize.DATE, allowNull: false, exposed: true };
            this.setColumnIfNotDefined(SequelizeModel.formatColumnName('created_at'), definition);
            this.setColumnIfNotDefined(SequelizeModel.formatColumnName('updated_at'), definition);
        }
    }

    /**
     * Add a column to definition if not defined
    */
    private setColumnIfNotDefined(name: string, definition: Column) {
        if (!this.definition.columns[name])
            this.definition.columns[name] = definition;
    }

    get columns() {
        return this.definition.columns
    }
    get name() {
        return this.definition.name
    }
    get sequelizeModel() {
        return this._sequelizeModel
    }
    get gqlModel() {
        return this._gqlModel
    }
}

export default Model
