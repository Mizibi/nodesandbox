import { ApolloServer } from 'apollo-server';
import { Production } from '@env/index';

import schema from '@schema/index'

export default new ApolloServer({
    schema,
    debug: !Production,
    introspection: true,
    playground: true,
});
