import 'dotenv/config';
import { WebClientURL, Port } from '@env/index';

import server from './api';

async function main() {
    console.log('Starting Server');
    await server.listen(Port);
    console.log(`Server started at ${WebClientURL}:${Port}`)
}

main().catch(console.error);
