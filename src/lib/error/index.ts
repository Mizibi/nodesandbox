import { ServerError } from '@emzeh/api-base';

export class MalformedSchema extends ServerError {
    constructor(message: string) {
        super('MalformedSchema', message);
    }
}
export class NotImplemented extends ServerError {
    constructor(message: string) {
        super('NotImplemented', message);
    }
}
