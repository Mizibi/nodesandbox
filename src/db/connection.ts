import { Sequelize } from 'sequelize'
import { Production, DatabaseURL } from '@env/index'

export default new Sequelize(DatabaseURL, {
    benchmark: !Production,
});
