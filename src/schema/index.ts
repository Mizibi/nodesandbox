import { GraphQLSchema, GraphQLObjectType } from 'graphql';
import * as models from '@definitions/models';
import query from './query';
import mutation from './mutation';

// Generate types from models
const types = Object.values(models)
    .map(model => model.gqlModel.model as GraphQLObjectType)

const schema = new GraphQLSchema({
    types,
    query,
    mutation,
});

export default schema;
