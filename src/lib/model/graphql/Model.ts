import { camelize } from 'inflection';
import {
    INTEGER,
    ENUM,
    STRING,
    TEXT,
    DATE,
    DATEONLY,
    BOOLEAN,
    FLOAT,
    DOUBLE,
    TIME,
} from 'sequelize'
import { GraphQLInt, GraphQLString, GraphQLBoolean, GraphQLFloat } from 'graphql'
import { MalformedSchema, NotImplemented } from '@lib/error';
import { Model } from '@lib/model'

import { GQLObject, GQLObjectFields } from './Object'
import { GraphQLDate } from './Date';

const SequelizeToGraphQLTypes = [
    {
        graphql: GraphQLInt,
        sequelize: [INTEGER],
    }, {
        graphql: GraphQLString,
        sequelize: [ENUM, STRING, TEXT],
    }, {
        graphql: GraphQLDate,
        sequelize: [DATE, DATEONLY, TIME],
    }, {
        graphql: GraphQLBoolean,
        sequelize: [BOOLEAN],
    }, {
        graphql: GraphQLFloat,
        sequelize: [FLOAT, DOUBLE],
    }
]

export class GQLModel {
    private _object: GQLObject

    constructor(parent: Model) {
        const { name } = parent
        const fields = this.getColumnFields(parent)


        this._object = new GQLObject({
            name,
            fields,
        }, parent)
    }

    /**
     * Get columns from model definition and call sequelizeTypeToGraphQLType
    */
    private getColumnFields(model: Model) {
        const { name, columns } = model

        const fields: GQLObjectFields = {}

        for (const key in columns) {
            const { exposed, type } = columns[key]

            if (exposed) {
                const exposedKey = camelize(key, true)
                if (fields[exposedKey])
                    throw new MalformedSchema(`${name}: field defined more than once: ${exposedKey}`)
                fields[exposedKey] = {
                    type: this.sequelizeTypeToGraphQLType(type),
                }
            }
        }
        return fields
    }

    /**
     * Transform sequelize type to GraphQL type
    */
    private sequelizeTypeToGraphQLType(type: any) {
        for (const { graphql, sequelize } of SequelizeToGraphQLTypes)
            for (const t of sequelize)
                if (type === t || type.constructor.key === t.key)
                    return graphql
        throw new NotImplemented(`Unhandled sequelize type: ${type}`)
    }

    get model() {
        return this._object
    }
}
