import { MandatoryEnvInt, MandatoryEnvString, OptionalEnvString } from '@emzeh/api-base'


export const NodeEnv = OptionalEnvString('NODE_ENV');
export const Production = (NodeEnv === 'production');

const nonNecessaryEnv = 'test' || 'ci'

export const Port = NodeEnv !== nonNecessaryEnv ?
    MandatoryEnvInt('PORT') :
    3000;

export const DatabaseURL = NodeEnv !== nonNecessaryEnv ?
    MandatoryEnvString('DATABASE_URL') :
    'postgres://postgres:test@localhost:5432/postgres';

export const WebClientURL = NodeEnv !== nonNecessaryEnv ?
    MandatoryEnvString('WEB_CLIENT_URL') :
    'http://localhost';
