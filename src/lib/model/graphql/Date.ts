import { GraphQLScalarType } from 'graphql';
import { Kind } from 'graphql/language'

export const GraphQLDate = new GraphQLScalarType({
    name: 'Date',
    serialize(value: any) {
        if (typeof (value) === 'string')
            value = new Date(value)
        return value.getTime() // value sent to the client
    },
    parseValue(value: any) {
        return new Date(value) // value from the client
    },
    parseLiteral(ast) {
        switch (ast.kind) {
            case Kind.INT:
                return parseInt(ast.value, 10) // ast value is always in string format
            default:
                return null
        }
    }
});

export default GraphQLDate
