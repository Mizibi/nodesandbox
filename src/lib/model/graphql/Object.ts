import {
    isType,
    isOutputType,
    GraphQLObjectType,
    GraphQLFieldConfigMap,
    GraphQLOutputType
} from 'graphql'
import { Model } from '@lib/model'

type Thunk<T> = (() => T) | T;
type Dictionary<T> = { [key: string]: T };

export type GQLObjectField = any | GraphQLOutputType;
export type GQLObjectFields = Dictionary<GQLObjectField>;

export interface GQLObjectArgs {
    name: string,
    fields: Thunk<GQLObjectFields>,
}

export class GQLObject extends GraphQLObjectType {
    constructor(args: GQLObjectArgs, private _parent: Model | null = null) {
        super({
            name: args.name,
            fields: GQLObject.processGqlFields(args.fields),
        })
    }

    /**
     * Process graphql fields
     */
    static processGqlFields(fields: GQLObjectFields): GraphQLFieldConfigMap<any, any> {
        const res: GraphQLFieldConfigMap<any, any> = {}
        Object.keys(fields).forEach((key) => {
            const field = fields[key]
            if (isType(field) && isOutputType(field))
                res[key] = { type: field }
            else
                res[key] = field
        })
        return res
    }

    get parent() {
        return this._parent
    }
}
