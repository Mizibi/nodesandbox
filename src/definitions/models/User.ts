import { STRING } from 'sequelize';
import { Model } from '@lib/model';

const User = new Model({
    name: 'User',
    table: 'users',
    columns: {
        firstname: { type: STRING, allowNull: true, exposed: true },
    },
    timestamps: true,
});

export default User;
