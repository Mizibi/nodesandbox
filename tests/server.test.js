import server from '../src/api/index'

const http = require('http')

describe('Server', function () {
    before((done) => {
        server.listen(3000).then(done())
    });
    describe('Server launch', () => {
        it('should start', async () => {
            await http.get("http://localhost:3000/")
        });
    });
});
