import 'dotenv/config'
import db from '@db/index';
const { DataTypes } = require('sequelize');

const queryInterface = db.getQueryInterface();

// function getTableNameFromTableInfo(tableInfo): string {
//     return tableInfo[Object.keys(tableInfo)[0]];
// }

async function main() {
    // const tablesInfo = await queryInterface.showAllSchemas()
    // const tablesNames = new Set<string>(tablesInfo.map(getTableNameFromTableInfo));
    // if (tablesNames.has('users')) {
    // }

    await queryInterface.dropTable('users')
    queryInterface.createTable('users', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        createdAt: {
            type: DataTypes.DATE
        },
        updatedAt: {
            type: DataTypes.DATE
        },
        firstName: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
    })
}

main()