import db from '@db/index';
import { underscore, camelize } from 'inflection';
import { Model as SequelizeModelType } from 'sequelize'
import { Model, ColumnBase, Columns } from '@lib/model'

type Dictionary<T> = { [key: string]: T };

const CamelcaseColumnNames = true;

interface SequelizeColumn extends ColumnBase {
    autoIncrement?: boolean,
    primaryKey?: boolean,
}

type SequelizeModelTypeWithParent = (new () => SequelizeModelType<any, any>) & { __parent: Model }

/**
 * Class SequelizeModel
 * Store sequelize model, fields and options
 *
 */
export class SequelizeModel {
    private _fields: Dictionary<SequelizeColumn> = {}
    private _options: any
    private _model: SequelizeModelTypeWithParent

    private _createdAtColumnName: string | null = null;
    private _updatedAtColumnName: string | null = null;

    /**
     * Transform column name with inflection
    */
    static formatColumnName(name: string): string {
        if (CamelcaseColumnNames)
            return camelize(name, true);
        return underscore(name);
    }

    constructor(parent: Model) {
        const { table, columns, timestamps } = parent.definition

        this.columnsToSequelizeFields(columns)

        this._createdAtColumnName = SequelizeModel.formatColumnName('created_at');
        this._updatedAtColumnName = SequelizeModel.formatColumnName('updated_at');

        this._options = {
            timestamps,
            createdAt: this._createdAtColumnName || undefined,
            updatedAt: this._updatedAtColumnName || undefined,
        }
        const model = db.define(table, this._fields, this._options)
        // Associate self to parent
        const mergedModel = Object.assign(model, { __parent: parent })
        this._model = mergedModel
    }

    /**
     * Transform definition columns to sequelize fields
    */
    private columnsToSequelizeFields(columns: Columns) {
        for (const name in columns) {
            const { type, allowNull: _allowNull, defaultValue } = columns[name];
            const allowNull = _allowNull === undefined ? true : _allowNull;

            this._fields[name] = {
                type,
                allowNull,
                defaultValue,
                ...this.generateFieldOption(name)
            };
        }
    }

    /**
     * Generate sequelize field primary key and autoIncrement field
    */
    private generateFieldOption(name: string) {
        return (name === 'id') ? {
            primaryKey: true,
            autoIncrement: true
        } : { primaryKey: false }
    }

    get fields() {
        return this._fields
    }
    get model() {
        return this._model
    }
}
